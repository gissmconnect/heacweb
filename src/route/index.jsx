import {
  Dashboard,
  MarksGrades,
  Profile,
  SmsInbox,
  EligiblePrograms,
  ViewAcceptOffers,
  FileUpload,
  ChoosePrograms,
} from "pages";

export const routes = [
  { path: `${process.env.PUBLIC_URL}/`, Component: Dashboard },
  { path: `${process.env.PUBLIC_URL}/inbox`, Component: SmsInbox },
  { path: `${process.env.PUBLIC_URL}/profile`, Component: Profile },
  { path: `${process.env.PUBLIC_URL}/marks-grades`, Component: MarksGrades },
  {
    path: `${process.env.PUBLIC_URL}/eligible-programs`,
    Component: EligiblePrograms,
  },
  {
    path: `${process.env.PUBLIC_URL}/choose-programs`,
    Component: ChoosePrograms,
  },
  {
    path: `${process.env.PUBLIC_URL}/view-accept-offers`,
    Component: ViewAcceptOffers,
  },
  {
    path: `${process.env.PUBLIC_URL}/file-upload`,
    Component: FileUpload,
  },
];
