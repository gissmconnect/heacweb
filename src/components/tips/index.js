import React from "react";
import { CheckCircle } from "react-feather";
import { translate } from "react-switch-lang";
import { Alert } from "reactstrap";

const Tips = (props) => {
  return (
    <Alert color="light">
      <div className="d-flex align-items-center">
        <CheckCircle size={18} className="mr-2 mb-2" />
        <h4 className="alert-heading">{"Tips"}</h4>
      </div>
      {props.children}
    </Alert>
  );
};

export default translate(Tips);
