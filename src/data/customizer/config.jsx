export class ConfigDB {
	static data = {
		settings: {
			layout_type: 'ltr',
			sidebar: {
				type: 'compact-wrapper',
				body_type: 'sidebar-icon'
			},
			sidebar_setting: 'default-sidebar',
		},
		color: {
			layout_version: 'light',
			color: 'color-1',
			primary_color: '#ec232e',
			secondary_color: '#888',
			mix_background_layout: 'light-only',
		},
		router_animation: 'fadeIn' // zoomfade, slidefade, fadebottom, fade, zoomout, none
	}
}
export default ConfigDB;




