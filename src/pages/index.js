export { default as Dashboard } from "./dashboard";
export { default as SmsInbox } from "./smsInbox";
export { default as Profile } from "./profile";
export { default as MarksGrades } from "./marksGrades";
export { default as EligiblePrograms } from "./eligiblePrograms";
export { default as ChoosePrograms } from "./choosePrograms";
export { default as ViewAcceptOffers } from "./viewAcceptOffers";
export { default as FileUpload } from "./fileUpload";
