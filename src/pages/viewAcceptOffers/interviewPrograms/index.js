import React from "react";
import { translate } from "react-switch-lang";
import { Card, CardHeader, Table } from "reactstrap";
import InterviewProgramRow from "./interviewProgramRow";

const interviewProgramsData = [
  {
    program_code: "US001",
    program_name: "Engineering",
    hei_code: "HEI Code",
    hei_name: "Quantity Surveying and Commercial management",
    offer_type: "Public Program",
    nomination_date: "30-08-2021",
  },
  {
    program_code: "US002",
    program_name: "Engineering",
    hei_code: "HEI Code",
    hei_name: "Quantity Surveying and Commercial management",
    offer_type: "Public Program",
    nomination_date: "30-08-2021",
  },
];

const InterviewPrograms = ({ offer }) => {
  return (
    <Card>
      <CardHeader>
        <h5>Interview Programs</h5>
      </CardHeader>
      <div className="table-responsive">
        <Table>
          <thead>
            <tr>
              <th scope="col">{"Program Code"}</th>
              <th scope="col">{"Program Name"}</th>
              <th scope="col">{"HEI Code"}</th>
              <th scope="col">{"HEI Name"}</th>
              <th scope="col">{"Offer Type"}</th>
              <th scope="col">{"Nomination date"}</th>
            </tr>
          </thead>
          <tbody>
            {interviewProgramsData.map((program) => (
              <InterviewProgramRow program={program} />
            ))}
          </tbody>
        </Table>
      </div>
    </Card>
  );
};

export default translate(InterviewPrograms);
