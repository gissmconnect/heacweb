import React from "react";
import { translate } from "react-switch-lang";

const InterviewProgramRow = ({ program }) => {
  return (
    <tr>
      <td>{program.program_code}</td>
      <td>{program.program_name}</td>
      <th>{program.hei_code}</th>
      <td>{program.hei_name}</td>
      <td>{program.offer_type}</td>
      <td>{program.nomination_date}</td>
    </tr>
  );
};

export default translate(InterviewProgramRow);
