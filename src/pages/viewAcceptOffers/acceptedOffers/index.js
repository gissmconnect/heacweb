import React from "react";
import { translate } from "react-switch-lang";
import { Card, CardHeader, Table } from "reactstrap";
import InterviewProgramRow from "./acceptedOfferRow";

const acceptedOffersData = [
  {
    round_number: 1,
    date_accepted_rejected: "31-08-2021 12:38:25",
    program_code: "US003",
    program_name: "Engineering",
    hei_name: "Quantity Surveying and Commercial management",
    offer_type: "Public Program",
    registered_institution: "",
    status: "Complete Registration at HEI",
    registration_status: "Registration Withdrawn Aug 31, 2021",
  },
  {
    round_number: 2,
    date_accepted_rejected: "31-08-2021 12:38:25",
    program_code: "US003",
    program_name: "Engineering",
    hei_name: "Quantity Surveying and Commercial management",
    offer_type: "Public Program",
    registered_institution: "",
    status: "Complete Registration at HEI",
    registration_status: "Registration Withdrawn Aug 31, 2021",
  },
];

const AcceptedOffers = ({ offer }) => {
  return (
    <Card>
      <CardHeader>
        <h5>Currently Accepted Offers</h5>
      </CardHeader>
      <div className="table-responsive">
        <Table>
          <thead>
            <tr>
              <th scope="col">{"Round Number"}</th>
              <th scope="col">{"Date Accepted/Rejected"}</th>
              <th scope="col">{"Program Code"}</th>
              <th scope="col">{"Program Name"}</th>
              <th scope="col">{"HEI Name"}</th>
              <th scope="col">{"Offer Type"}</th>
              <th scope="col">{"Registered Institution"}</th>
              <th scope="col">{"Status"}</th>
              <th scope="col">{"Registration Status/Date"}</th>
            </tr>
          </thead>
          <tbody>
            {acceptedOffersData.map((program) => (
              <InterviewProgramRow program={program} />
            ))}
          </tbody>
        </Table>
      </div>
    </Card>
  );
};

export default translate(AcceptedOffers);
