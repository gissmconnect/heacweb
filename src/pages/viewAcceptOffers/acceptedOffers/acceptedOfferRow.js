import React from "react";
import { translate } from "react-switch-lang";

const AcceptedOfferRow = ({ program }) => {
  return (
    <tr>
      <td>{program.round_number}</td>
      <td>{program.date_accepted_rejected}</td>
      <th>{program.program_code}</th>
      <td>{program.program_name}</td>
      <td>{program.hei_name}</td>
      <td>{program.offer_type}</td>
      <td>{program.registered_institution}</td>
      <td>{program.status}</td>
      <td>{program.registration_status}</td>
    </tr>
  );
};

export default translate(AcceptedOfferRow);
