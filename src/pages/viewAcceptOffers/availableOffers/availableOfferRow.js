import React from "react";
import { translate } from "react-switch-lang";
import { ButtonToggle } from "reactstrap";

const AvailableOfferRow = ({ offer }) => {
  return (
    <tr>
      <th scope="row">{offer.round_number}</th>
      <td>{offer.date_offered}</td>
      <td>{offer.reply_date}</td>
      <td>{offer.registration_end_date}</td>
      <td>{offer.program_code}</td>
      <td>{offer.program_name}</td>
      {/* <td>{offer.hei_name}</td> */}
      <td>{offer.offer_type}</td>
      <td>{offer.country}</td>
      <td>{offer.status}</td>
      <td>
        <div className="d-flex">
          <ButtonToggle className="btn-xs mr-2" color="success">
            Accept
          </ButtonToggle>{" "}
          <ButtonToggle className="btn-xs" color="danger">
            Reject
          </ButtonToggle>
        </div>
      </td>
    </tr>
  );
};

export default translate(AvailableOfferRow);
