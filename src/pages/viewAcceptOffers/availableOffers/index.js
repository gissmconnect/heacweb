import React from "react";
import { translate } from "react-switch-lang";
import { Card, CardHeader, Table } from "reactstrap";
import AvailableOfferRow from "./availableOfferRow";

const availableData = [
  {
    round_number: 1,
    date_offered: "30-08-2021",
    reply_date: "10-09-2021",
    registration_end_date: "30-09-2021",
    program_code: "US001",
    program_name: "Engineering",
    hei_name: "Quantity Surveying and Commercial management",
    offer_type: "Public Program",
    country: "Oman",
    status: "Open",
  },
  {
    round_number: 2,
    date_offered: "31-08-2021",
    reply_date: "11-09-2021",
    registration_end_date: "28-09-2021",
    program_code: "US002",
    program_name: "Engineering",
    hei_name: "Quantity Surveying and Commercial management",
    offer_type: "Public Program",
    country: "Oman",
    status: "Open",
  },
];

const AvailableOffers = ({ offer }) => {
  return (
    <Card>
      <CardHeader>
        <h5>Available Offers</h5>
      </CardHeader>
      <div className="table-responsive">
        <Table>
          <thead>
            <tr>
              <th scope="col">{"Round Number"}</th>
              <th scope="col">{"Date Offered"}</th>
              <th scope="col">{"Reply Date"}</th>
              <th scope="col">{"Registration End Date"}</th>
              <th scope="col">{"Program Code"}</th>
              <th scope="col">{"Program Name"}</th>
              {/* <th scope="col">{"HEI Name"}</th> */}
              <th scope="col">{"Offer Type"}</th>
              <th scope="col">{"Country"}</th>
              <th scope="col">{"Status"}</th>
              <th scope="col">{"Accept/Reject"}</th>
            </tr>
          </thead>
          <tbody>
            {availableData.map((offer) => (
              <AvailableOfferRow offer={offer} />
            ))}
          </tbody>
        </Table>
      </div>
    </Card>
  );
};

export default translate(AvailableOffers);
