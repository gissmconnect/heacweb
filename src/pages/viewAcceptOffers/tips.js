import { Tips } from "components";
import React from "react";
import { translate } from "react-switch-lang";

const ViewAcceptOffersTips = () => {
  return (
    <Tips>
      <p>Shown below are any new offers made to you.</p>
      <p>
        When receiving a (SMS) message informing you of your eligibility to sit
        an interview or an admission test for a certain programs, this programme
        will not appear to you as an "offer" unless you pass the tests and your
        eligibility is within number of seats
      </p>
      <br />
      <p>
        If you do not accept, the offer will be withdrawn after the last date.
        <br />
        When failing to complete the registration procedures at the institute in
        which you have been accepted, your offer will automatically be
        cancelled.
      </p>
      <br />
      <p>
        Offer for Low Income Scholarships are conditional on you providing
        relevant evidence to the HEI.
      </p>
      <br />
      <p>
        You can change the "acceptance" to "rejection" or vice versa during the
        acceptance period.
      </p>
    </Tips>
  );
};

export default translate(ViewAcceptOffersTips);
