import { Breadcrumb } from "components";
import React, { Fragment } from "react";
import { translate } from "react-switch-lang";
import { Button, Col } from "reactstrap";
import AvailableOffers from "./availableOffers";
import InterviewPrograms from "./interviewPrograms";
import AcceptedOffers from "./acceptedOffers";
import ViewAcceptOffersTips from "./tips";
import { Printer } from "react-feather";

const ViewAcceptOffers = (props) => {
  return (
    <Fragment>
      <Breadcrumb title={props.t("View And Accept Offers of Places")} />
      <Col>
        <ViewAcceptOffersTips />
      </Col>
      <Col xl="4" lg="12 box-col-12 xl-100" className="mb-4">
        <AvailableOffers />
        <InterviewPrograms />
        <AcceptedOffers />
        <Button outline color="primary">
          <Printer className="mr-2" size={12} />
          Print
        </Button>
      </Col>
    </Fragment>
  );
};

export default translate(ViewAcceptOffers);
