import { Breadcrumb } from "components";
import React, { Fragment } from "react";
import { translate } from "react-switch-lang";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
} from "reactstrap";
import { ExternalLink } from "react-feather";
import { Link } from "react-router-dom";

const Dashboard = (props) => {
  const links = [
    {
      title: "Students Eligible Programs",
      opening_date: "01 May 2021 07:30",
      closing_date: "01 September 2021 07:30",
      status: "Closed",
      path: "/eligible-programs",
    },
    {
      title: "View or Enter Public programs, Scholarship and Grants",
      opening_date: "01 August 2021 12:00",
      closing_date: "01 September 2021 12:00",
      status: "Closed",
      path: "/choose-programs",
    },
    {
      title: "View and Accept Offers of Places",
      opening_date: "10 September 2021 07:30",
      closing_date: "30 September 2021 07:30",
      status: "Open",
      path: "/view-accept-offers",
    },
  ];

  return (
    <Fragment>
      <Breadcrumb title={props.t("Dashboard")} />
      <Container fluid={true}>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <h6>
                  {props.t("Welcome")},{" "}
                  <span className="txt-success">
                    Tamadher Hilal Ali Al - Ghafri
                  </span>
                </h6>
                <span>
                  Last Login <i>Wednesday, 04/08/2021, 06:05:35 PM</i>
                </span>
              </CardHeader>
              <CardBody className="pt-4">
                <div className="user-status table-responsive">
                  <Table borderless>
                    <tbody>
                      {links.map((data) => (
                        <tr style={{ cursor: "pointer" }} key={data.title}>
                          <Link to={`${process.env.PUBLIC_URL}${data.path}`}>
                            <td className="p-0 pb-1">
                              <div className="align-middle image-sm-size">
                                <div className="d-inline-block">
                                  <h6 className="mb-0">
                                    {data.title}{" "}
                                    <span>
                                      <ExternalLink size={15} />
                                    </span>
                                  </h6>
                                  <span className="text-muted digits">
                                    <i>
                                      {data.opening_date} to {data.closing_date}
                                    </i>
                                  </span>
                                </div>
                              </div>
                            </td>
                            <td>
                              <div
                                className={`span badge badge-pill pill-badge-${
                                  data.status === "Open" ? "success" : "primary"
                                }`}
                              >
                                {data.status}
                              </div>
                            </td>
                          </Link>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
};

export default translate(Dashboard);
