import { Breadcrumb } from "components";
import React, { Fragment } from "react";
import { MessageSquare } from "react-feather";
import { translate } from "react-switch-lang";
import { Col, Card, CardHeader, CardBody } from "reactstrap";

const SmsInbox = (props) => {
  return (
    <Fragment>
      <Breadcrumb title={props.t("SMS Inbox")} />
      <Col xl="4" lg="12 box-col-12 xl-100">
        <div>
          <Card>
            <CardBody>
              <ul className="crm-activity">
                <li className="media">
                  <span className="mr-3 font-secondary">
                    <MessageSquare />
                  </span>
                  <div className="align-self-center media-body">
                    <h6 className="mt-0">
                      {"Established fact that a reader will be distracted "}
                    </h6>
                    <ul className="dates">
                      <li className="digits">{"10 September 2021"}</li>
                      <li className="digits">{props.t("Mobile")}: 1234567890</li>
                    </ul>
                  </div>
                </li>
                <li className="media">
                  <span className="mr-3 font-secondary">
                    <MessageSquare />
                  </span>
                  <div className="align-self-center media-body">
                    <h6 className="mt-0">
                      {"Any desktop publishing packages and web page editors."}
                    </h6>
                    <ul className="dates">
                      <li className="digits">{"29 August 2021"}</li>
                      <li className="digits">{props.t("Mobile")}: 1234567890</li>
                    </ul>
                  </div>
                </li>
                <li className="media">
                  <span className="mr-3 font-secondary">
                    <MessageSquare />
                  </span>
                  <div className="align-self-center media-body">
                    <h6 className="mt-0">
                      {"There isn't anything embarrassing hidden."}
                    </h6>
                    <ul className="dates">
                      <li className="digits">{"28 August 2021"}</li>
                      <li className="digits">{props.t("Mobile")}: 1234567890</li>
                    </ul>
                  </div>
                </li>
                <li className="media">
                  <span className="mr-3 font-secondary">
                    <MessageSquare />
                  </span>
                  <div className="align-self-center media-body">
                    <h6 className="mt-0">
                      {"Contrary to popular belief, Lorem Ipsum is not simply."}{" "}
                    </h6>
                    <ul className="dates">
                      <li className="digits">{"28 August 2021"}</li>
                      <li className="digits">{props.t("Mobile")}: 1234567890</li>
                    </ul>
                  </div>
                </li>
                <li className="media">
                  <span className="mr-3 font-secondary">
                    <MessageSquare />
                  </span>
                  <div className="align-self-center media-body">
                    <h6 className="mt-0">
                      {"H-Code - A premium portfolio template from ThemeZaa"}{" "}
                    </h6>
                    <ul className="dates">
                      <li className="digits">{"25 August 2021"}</li>
                      <li className="digits">{props.t("Mobile")}: 1234567890</li>
                    </ul>
                  </div>
                </li>
              </ul>
            </CardBody>
          </Card>
        </div>
      </Col>
    </Fragment>
  );
};

export default translate(SmsInbox);
