import { Breadcrumb } from "components";
import React, { Fragment } from "react";
import { translate } from "react-switch-lang";
import { Card, CardBody, CardHeader, Col } from "reactstrap";
import FileUploadTips from "./tips";
import FileUploadRow from "./fileUploadRow";

const files = [
  {
    label: "Copy of civil Id or passport",
    hint: "",
  },
  {
    label: "Transcript of General Education Diploma or Equivalent",
    hint: "",
  },
  {
    label: "Equivalency Certificate from MOHE",
    hint: "Equivalent Certificate is not required from students who student is the public schools of GCC countries.",
  },
  {
    label: "Ability Skills Certificate",
    hint: "Students having only KSA Certificate",
  },
  {
    label: "Achievement Test Certificate",
    hint: "Students having only KSA Certificate",
  },
  {
    label: "Grade 10 Certificate",
    hint: "Students exam country is America",
  },
  {
    label: "Grade 11 Certificate",
    hint: "Students exam country is America",
  },
];

const FileUpload = (props) => {
  return (
    <Fragment>
      <Breadcrumb title={props.t("File Upload")} />
      <Col>
        <FileUploadTips />
      </Col>
      <Col xl="4" lg="12 box-col-12 xl-100" className="mb-4">
        <Card>
          <CardHeader>
            <h5>File Upload</h5>
          </CardHeader>
          <CardBody>
            {files.map((file, index) => (
              <FileUploadRow file={file} index={index}/>
            ))}
          </CardBody>
        </Card>
      </Col>
    </Fragment>
  );
};

export default translate(FileUpload);
