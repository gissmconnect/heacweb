import React, { useRef, useState } from "react";
import { Edit, Eye, Info, Upload, X } from "react-feather";
import { translate } from "react-switch-lang";
import {
  ButtonToggle,
  Col,
  FormGroup,
  Input,
  Label,
  Tooltip,
} from "reactstrap";

const FileUploadRow = ({ file, index }) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const [selectedFile, setSelectedFile] = useState("");

  const ref = useRef();

  const toggle = () => setTooltipOpen(!tooltipOpen);

  const onFileChange = (event) => {
    console.log("onFileChnage files", event.target.files)
    setSelectedFile(event.target.value);
  };

  const reset = () => {
    // @ts-ignore
    ref.current.value = "";
    setSelectedFile(null);
  };

  return (
    <>
      <div className="form-row mb-1">
        <Col>
          <FormGroup className="row mt-2">
            <Label className="col-sm-5 col-form-label">
              {file.label}
              {file.hint ? (
                <Info
                  id={`tooptip_${index}`}
                  className="info-icon pt-1 ml-2"
                  size={15}
                />
              ) : (
                ""
              )}
            </Label>
            <Col sm="4">
              <Input
                innerRef={ref}
                value={selectedFile}
                type="file"
                onChange={onFileChange}
              />
            </Col>
            {selectedFile ? (
              <Col sm="3" className="d-flex align-items-center">
                <X
                  onClick={reset}
                  color="red"
                  className="mr-3 cursor-pointer"
                />
                {/* <a target="_blank" href={selectedFile}> */}
                <Eye
                  onClick={() => {
                    window.open(selectedFile);
                  }}
                  className="mr-3 info-color cursor-pointer"
                />
                {/* </a> */}
                <ButtonToggle className="btn-xs" color="success">
                  <Upload size={10} className="mr-2" />
                  Upload
                </ButtonToggle>{" "}
              </Col>
            ) : null}
          </FormGroup>
        </Col>
      </div>

      {file.hint ? (
        <Tooltip
          placement="top"
          isOpen={tooltipOpen}
          autohide={false}
          target={`tooptip_${index}`}
          toggle={toggle}
        >
          {file.hint}
        </Tooltip>
      ) : null}
    </>
  );
};

export default translate(FileUploadRow);
