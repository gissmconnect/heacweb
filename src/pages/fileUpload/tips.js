import React from "react";
import { translate } from "react-switch-lang";
import { Tips } from "components";

const FileUploadTips = () => {
  return (
    <Tips>
      <p>Please to upload all the required document.</p>
      <p>All document should be PDF file (.pdf) file.</p>
      <p>Each document can not exceed 512 KB in size.</p>
      <p className="text-danger">
        For American school system students/insert your school subjects for
        grade 12, plus science subjects (Chemistry - Physics- Biology) of grades
        10 and 11 if you want, and attach the transcripts in the fields of
        grades 10 and 11.
      </p>
    </Tips>
  );
};

export default translate(FileUploadTips);
