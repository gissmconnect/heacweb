// @ts-nocheck
import React, { useState } from "react";
import { Container, Row, Col, Card, CardHeader, Table } from "reactstrap";
import programsData from "./data";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import ProgramRow from "./programRow";

const wishlistIdDivider = "_d_";

const createWishlistId = (programId, columnId) =>
  `${programId}${wishlistIdDivider}${columnId}`;

const Programs = () => {
  const [state, setState] = useState(programsData);

  const onDragStart = (result) => {
    //
  };

  const onDragUpdate = () => {
    //
  };

  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    if (!destination) return;
    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    )
      return;

    const start = state.columns[source.droppableId];
    const finish = state.columns[destination.droppableId];

    if (start === finish) {
      const programs = Array.from(start.programs);
      const draggableProgram = programs.find((p) => p.id === draggableId);
      programs.splice(source.index, 1);
      programs.splice(destination.index, 0, draggableProgram);

      const newColumn = {
        ...start,
        programs: programs,
      };

      const newState = {
        ...state,
        columns: {
          ...state.columns,
          [newColumn.id]: newColumn,
        },
      };
      setState(newState);
      return;
    }

    // Moving from one list to another
    const startPrograms = Array.from(start.programs);
    const finishPrograms = Array.from(finish.programs);

    const draggableProgram = startPrograms.find((p) => p.id === draggableId);

    const newProgramId = createWishlistId(draggableProgram.id, finish.id);

    const isAlreadyExist = finishPrograms.findIndex(
      (p) => p.id === newProgramId
    );

    if (isAlreadyExist !== -1) {
      finishPrograms.splice(isAlreadyExist, 1);
    }

    console.log("Programs onDragEnd", {
      start,
      finish,
      draggableId,
      finishPrograms,
      startPrograms,
      draggableProgram,
      isAlreadyExist,
    });

    finishPrograms.splice(destination.index, 0, {
      ...draggableProgram,
      id: newProgramId,
    });
    const newFinish = {
      ...finish,
      programs: finishPrograms,
    };

    const newState = {
      ...state,
      columns: {
        ...state.columns,
        [newFinish.id]: newFinish,
      },
    };

    setState(newState);
  };

  const removeProgramFromWishlist = (columnId, programId) => {
    const selectedColumn = { ...state.columns[columnId] };
    const programs = selectedColumn.programs;
    const programIndex = programs.findIndex((p) => p.id === programId);
    programs.splice(programIndex, 1);

    const newState = {
      ...state,
      columns: {
        ...state.columns,
        [columnId]: selectedColumn,
      },
    };
    setState(newState);
  };

  console.log("Programs Render", { state });

  return (
    <Container fluid={true} className="choose-programs">
      <Row>
        <DragDropContext
          onDragStart={onDragStart}
          onDragUpdate={onDragUpdate}
          onDragEnd={onDragEnd}
        >
          {Object.keys(state.columns).map((columnId, columnIndex) => {
            const column = state.columns[columnId];
            const programs = column.programs;
            if(!columnIndex) return;
            return (
              <Col key={column.id} sm="12">
                <Card>
                  <CardHeader>
                    <h5>{column.title}</h5>
                  </CardHeader>

                  <div className="table-responsive">
                    <Table className="table-de">
                      <thead>
                        <tr>
                          <th scope="col">{"S.N."}</th>
                          <th scope="col">{"Code"}</th>
                          <th scope="col">{"Program Name"}</th>
                          <th />
                          {columnIndex ? <th /> : null}
                        </tr>
                      </thead>
                      <Droppable
                        isDropDisabled={!columnIndex}
                        droppableId={column.id}
                      >
                        {(provided, snapshot) => (
                          <tbody
                            ref={provided.innerRef}
                            {...provided.droppableProps}
                          >
                            {programs.length ? (
                              programs.map((program, index) => (
                                <ProgramRow
                                  key={program.key}
                                  columnIndex={columnIndex}
                                  index={index}
                                  program={program}
                                  removeProgram={removeProgramFromWishlist}
                                  columnId={column.id}
                                />
                              ))
                            ) : (
                              <ProgramRow
                                program={{}}
                                columnId={column.id}
                                isEmpty
                              />
                            )}
                            {provided.placeholder}
                          </tbody>
                        )}
                      </Droppable>
                    </Table>
                  </div>
                </Card>
              </Col>
            );
          })}
        </DragDropContext>
      </Row>
    </Container>
  );
};

export default Programs;
