import React, { Fragment } from "react";
import { Breadcrumb } from "components";
import { translate } from "react-switch-lang";
import Programs from "./programs";
import EligibleProgramsTips from "./tips";
import { Col } from "reactstrap";

const EligiblePrograms = (props) => {
  return (
    <Fragment>
      <Breadcrumb parent="" title="Students Eligible Programs" />
      <Col>
        <EligibleProgramsTips />
      </Col>
      <Programs />
    </Fragment>
  );
};

export default translate(EligiblePrograms);
