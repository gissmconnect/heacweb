import React from "react";
import { translate } from "react-switch-lang";
import { Tips } from "components";

const EligibleProgramsTips = () => {
  return (
    <Tips>
      <p>You cannot depend on these programs until final marks are updated.</p>
    </Tips>
  );
};

export default translate(EligibleProgramsTips);
