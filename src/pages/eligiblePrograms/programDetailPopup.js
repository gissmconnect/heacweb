// @ts-nocheck
import React, { useState } from "react";
import { PopoverHeader, PopoverBody, UncontrolledPopover } from "reactstrap";
import { Info } from "react-feather";

const ProgramDetailPopup = (props) => {
  const { program={} } = props;
  const [popover, setPopover] = useState(false);

  const OffsetToggle = () => setPopover(!popover);

  const toolTipId = `tool_tip_${program.id}`;

  return (
    <>
      <Info id={toolTipId} className="info-icon" size={20} />
      <UncontrolledPopover
        placement="top"
        isOpen={popover}
        target={toolTipId}
        toggle={OffsetToggle}
        trigger="hover"
        className="program-detail-popup"
      >
        <PopoverHeader>{program.name}</PopoverHeader>
        <PopoverBody>
          <div className="program-detail-modal-body">
            <div className="program-detail-row">
              <span>PROGRAM CODE:</span> <b>{program.code}</b>
            </div>
            <div className="program-detail-row">
              <span> HEI NAMW: </span>
              <b>Arab Open University</b>
            </div>
            <div className="program-detail-row">
              <span> OFFER TYPE NAME: </span>
              <b>Public Internal Grant Public</b>
            </div>
            <div className="program-detail-row">
              <span> SPONSOR NAME:</span> <b>Arab Open University</b>
            </div>
            <div className="program-detail-row">
              <span> COUNTRY OF STUDY: </span>
              <b>Oman</b>
            </div>
            <div className="program-detail-row">
              <span> QUALIFICATION:</span> <b>Bachelor of equivalent</b>
            </div>
          </div>
        </PopoverBody>
      </UncontrolledPopover>
    </>
  );
};

export default ProgramDetailPopup;
