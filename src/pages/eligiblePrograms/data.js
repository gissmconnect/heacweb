const programsData = {
  tasks: {
    task1: { id: "task1", content: "Take out the garbage" },
    task2: { id: "task2", content: "Watch my favorite show" },
    task3: { id: "task3", content: "Charge my phone" },
    task4: { id: "task4", content: "Cook dinner" },
    task5: { id: "task5", content: "Task 5" },
    task6: { id: "task6", content: "Task 6" },
  },
  columns: {
    all_programs: {
      id: "all_programs",
      title: "All Programs",
      taskIds: ["task1", "task2", "task3", "task4"],
      programs: [],
    },
    whish_list: {
      id: "whish_list",
      title: "Wishlist",
      taskIds: ["task5"],
      programs: [
        { id: "task1", name: "Law", code: "BS077" },
        {
          id: "task2",
          name: "Team Entrepreneurship - Management - Finance and Accounting",
          code: "BS216",
        },
        { id: "task3", name: "Industrial chemistry", code: "BS085" },
        {
          id: "task4",
          name: "Bachelor of Technology in Electrical Engineering",
          code: "BS087",
        },
        {
          id: "task5",
          name: "Quantity Surveying and Commercial management",
          code: "BS088",
        },
        {
          id: "task6",
          name: "Internet and information technology-Cybersecurity",
          code: "BS089",
        },
        {
          id: "task7",
          name: "Bachelor of Education Science and MAthematics (Mathematics)",
          code: "US016",
        },
        {
          id: "task8",
          name: "Bachelor of Education Science and MAthematics (Chemistry)",
          code: "US017",
        },
        {
          id: "task9",
          name: "Bachelor of Education Science and MAthematics (Physics)",
          code: "US018",
        },
        { id: "task10", name: "College of Education (English)", code: "US019" },
        { id: "task11", name: "College of Education (Arabic)", code: "US020" },
      ],
    },
  },
};

export default programsData;
