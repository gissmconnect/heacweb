import React, { Fragment } from "react";
import { Breadcrumb } from "components";
import { Container, Col, Card, CardHeader, Table, Badge } from "reactstrap";
import { translate } from "react-switch-lang";

const MarksGrades = (props) => {
  return (
    <Fragment>
      <Breadcrumb parent="" title="Marks & Grades" />
      <Container fluid={true}>
        <Card>
          <CardHeader>
            <div className="display-flex row align-items-center justify-content-between">
              <h5>{props.t("Marks_Grades")}</h5>
              <div>
                Status:{" "}<Badge color="success">{props.t("Passed")}</Badge>
              </div>
            </div>
          </CardHeader>
          <div className="table-responsive">
            <Table>
              <thead>
                <tr>
                  <th scope="col">{props.t("Subject_Name")}</th>
                  <th scope="col">{props.t("Grade")}</th>
                  <th scope="col">{props.t("Marks_Percentage")}</th>
                  <th scope="col">{props.t("Status")}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{"Physics"}</td>
                  <td>{"B"}</td>
                  <td>{"81"}</td>
                  <td>{""}</td>
                </tr>
                <tr>
                  <td>{"Social Studies (Civis)"}</td>
                  <td>{"A"}</td>
                  <td>{"100"}</td>
                  <td>{""}</td>
                </tr>
                <tr>
                  <td>{"Islamic Education"}</td>
                  <td>{"A"}</td>
                  <td>{"98"}</td>
                  <td>{""}</td>
                </tr>
                <tr>
                  <td>{"Arabic Language"}</td>
                  <td>{"A"}</td>
                  <td>{"93"}</td>
                  <td>{""}</td>
                </tr>
                <tr>
                  <td>{"Pure Math"}</td>
                  <td>{"B"}</td>
                  <td>{"83"}</td>
                  <td>{""}</td>
                </tr>
                <tr>
                  <td>{"English Language"}</td>
                  <td>{"B"}</td>
                  <td>{"81"}</td>
                  <td>{""}</td>
                </tr>
                <tr>
                  <td>{"Chemistry"}</td>
                  <td>{"B"}</td>
                  <td>{"84"}</td>
                  <td>{""}</td>
                </tr>
                <tr>
                  <td>{"Biology"}</td>
                  <td>{"C+"}</td>
                  <td>{"75"}</td>
                  <td>{""}</td>
                </tr>
                <tr>
                  <td>{"Physical Education"}</td>
                  <td>{"A"}</td>
                  <td>{"99"}</td>
                  <td>{""}</td>
                </tr>
              </tbody>
            </Table>
          </div>
        </Card>
      </Container>
    </Fragment>
  );
};

export default translate(MarksGrades);
