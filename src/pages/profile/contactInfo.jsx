import React from "react";
import { useForm } from "react-hook-form";
import { Row, Col, Form, Label, Input, FormGroup } from "reactstrap";

const ContactInfo = () => {
  const { register, handleSubmit, formState } = useForm();

  const { errors = {} } = formState;

  const onSubmit = (data) => {
    if (data !== "") {
      alert("You submitted the form and stuff!");
    } else {
      errors.showMessages();
    }
  };

  return (
    <Row className="mt-5">
      <Col>
        <Form className="needs-validation" onSubmit={handleSubmit(onSubmit)}>
          <div className="form-row mb-1">
            <Col>
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Select Your Country Telephone Code
                </Label>
                <Col sm="5">
                  <Input
                    type="select"
                    name="countryCode"
                    className="form-control btn-pill digits"
                  >
                    <option>{"+968 Oman"}</option>
                    <option>{"+1 USA"}</option>
                    <option>{"+52 Mexico"}</option>
                  </Input>
                </Col>
              </FormGroup>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col>
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">* GSM Number</Label>
                <Col sm="5">
                  <Input
                    className="form-control"
                    name="gsmNumber"
                    type="text"
                    placeholder="GSM Number"
                    {...register("gsmNumber", { required: true })}
                  />
                  <span className="info-text">
                    Will be used by HEAC to send important SMS. Do not enter
                    landline number here. Local Numbers is preferred, if you
                    entered an international number, please enter the country
                    code.
                  </span>
                  <span>{errors.gsmNumber && "GSM Number is required"}</span>
                </Col>
              </FormGroup>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col>
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Home Phone Number
                </Label>
                <Col sm="5">
                  <Input
                    className="form-control"
                    name="homePhoneNumber"
                    type="text"
                    placeholder="Home Phone Number"
                    {...register("homePhoneNumber", { required: true })}
                  />
                  <span className="info-text">
                    HEAC my call you at this number.
                  </span>
                  <span>
                    {errors.homePhoneNumber && "Home Phone Number is required"}
                  </span>
                </Col>
              </FormGroup>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col>
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">Email Address</Label>
                <Col sm="5">
                  <Input
                    className="form-control"
                    name="email"
                    type="text"
                    placeholder="Email"
                    {...register("email", { required: true })}
                  />
                  <span>{errors.email && "Email is required"}</span>
                </Col>
              </FormGroup>
            </Col>
          </div>
        </Form>
      </Col>
    </Row>
  );
};

export default ContactInfo;
