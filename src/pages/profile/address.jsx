import React from "react";
import { useForm } from "react-hook-form";
import { Row, Col, Form, Label, Input } from "reactstrap";

const Address = () => {
  const { register, handleSubmit, formState } = useForm();

  const { errors = {} } = formState;

  const onSubmit = (data) => {
    if (data !== "") {
      alert("You submitted the form and stuff!");
    } else {
      errors.showMessages();
    }
  };

  return (
    <Row className="mt-5">
      <Col sm="12">
        <Form className="needs-validation" onSubmit={handleSubmit(onSubmit)}>
          <div className="form-row mb-1">
            <Col md="6 mb-3">
              <Label htmlFor="validationCustomUsername">
                School Region Name
              </Label>
              <Input
                className="form-control"
                name="schoolRegion"
                type="text"
                value={"Al Batinah North"}
                placeholder="School Region Name"
                {...register("schoolRegion", { required: true })}
              />
              <span>
                {errors.schoolRegion && "School Region Name required"}
              </span>
            </Col>
            <Col md="6 mb-3">
              <Label htmlFor="validationCustom01">School Wilayat Name</Label>
              <Input
                className="form-control"
                name="wilayatName"
                type="text"
                value="Sohar"
                placeholder="chool Wilayat Name"
                {...register("wilayatName", { required: true })}
              />
              <span>
                {errors.wilayatName && "chool Wilayat Name is required"}
              </span>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col md="6 mb-3">
              <Label htmlFor="validationCustom01">Student Wilayat</Label>
              <Input
                className="form-control"
                name="studentWilayat"
                type="text"
                value="Nasser"
                placeholder="Student Wilayat"
                {...register("studentWilayat", { required: true })}
              />
              <span>
                {errors.studentWilayat && "Student Wilayat name is required"}
              </span>
            </Col>
            <Col md="6 mb-3">
              <Label htmlFor="validationCustom02">Village Name</Label>
              <Input
                className="form-control"
                name="villageName"
                type="text"
                placeholder="Fourth name"
                {...register("villageName")}
              />
            </Col>
          </div>
        </Form>
      </Col>
    </Row>
  );
};

export default Address;
