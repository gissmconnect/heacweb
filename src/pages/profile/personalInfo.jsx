import React from "react";
import { useForm } from "react-hook-form";
import { Row, Col, Form, Label, Input, Badge } from "reactstrap";

const PersonalInfo = () => {
  const { register, handleSubmit, formState } = useForm();

  const { errors = {} } = formState;

  const onSubmit = (data) => {
    if (data !== "") {
      alert("You submitted the form and stuff!");
    } else {
      errors.showMessages();
    }
  };

  return (
    <Row className="mt-5">
      <Col sm="12">
        <Form className="needs-validation" onSubmit={handleSubmit(onSubmit)}>
          <div className="form-row mb-1">
            <Col md="4 mb-3">
              <Label htmlFor="validationCustomUsername">Civil Number</Label>
              <Input
                className="form-control"
                name="civilNumber"
                type="text"
                value={"15383761"}
                placeholder="Civil Number"
                disabled
                {...register("civilNumber", { required: true })}
              />
              <span>{errors.civilNumber && "Civi Number required"}</span>
            </Col>
            <Col md="4 mb-3">
              <Label htmlFor="validationCustom01">First Name</Label>
              <Input
                className="form-control"
                name="firstName"
                type="text"
                value="Rayan"
                placeholder="First name"
                {...register("firstName", { required: true })}
              />
              <span>{errors.firstName && "First name is required"}</span>
              <div className="valid-feedback">{"Looks good!"}</div>
            </Col>
            <Col md="4 mb-3">
              <Label htmlFor="validationCustom02">Second Name</Label>
              <Input
                className="form-control"
                name="secondName"
                type="text"
                value="Hamed"
                placeholder="Second name"
                {...register("secondName", { required: true })}
              />
              <span>{errors.secondName && "Second name is required"}</span>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col md="4 mb-3">
              <Label htmlFor="validationCustom01">Third Name</Label>
              <Input
                className="form-control"
                name="thirdName"
                type="text"
                value="Nasser"
                placeholder="Third name"
                {...register("thirdName", { required: true })}
              />
              <span>{errors.thirdName && "Third name is required"}</span>
              <div className="valid-feedback">{"Looks good!"}</div>
            </Col>
            <Col md="4 mb-3">
              <Label htmlFor="validationCustom02">Fourth Name</Label>
              <Input
                className="form-control"
                name="fourthName"
                type="text"
                placeholder="Fourth name"
                {...register("fourthName")}
              />
            </Col>
            <Col md="4 mb-3">
              <Label htmlFor="validationCustom02">Family Name</Label>
              <Input
                className="form-control"
                name="familyName"
                type="text"
                value="Al Mamari"
                placeholder="Family name"
                {...register("familyName")}
              />
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col md="3 mb-3">
              <Label htmlFor="validationCustom03">Gender</Label>
              <Input
                className="form-control"
                name="gender"
                type="text"
                placeholder="Gender"
                value={"Female"}
                disabled
                {...register("gender", { required: true })}
              />
              <span>{errors.city && "Please provide a valid city"}</span>
              <div className="invalid-feedback">
                {"Please provide a valid city."}
              </div>
            </Col>
            <Col md="3 mb-3">
              <Label htmlFor="validationCustom04">Age</Label>
              <Input
                className="form-control"
                id="validationCustom04"
                name="age"
                type="text"
                placeholder="Age"
                value="18"
                disabled
                {...register("age", { required: true })}
              />
              <span>{errors.state && "Please provide a valid state."}</span>
              <div className="invalid-feedback">
                {"Please provide a valid state."}
              </div>
            </Col>
            <Col md="4 mb-3">
              <Label htmlFor="validationCustom05">Date Of Birth</Label>
              <Input
                className="form-control"
                id="validationCustom05"
                name="dob"
                type="text"
                placeholder="Date of Birth"
                value="12/05/2003"
                disabled
                {...register("dob", { required: true })}
              />
              <span>{errors.zip && "Please provide a valid zip."}</span>
              <div className="invalid-feedback">
                {"Please provide a valid zip."}
              </div>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col>
              <div className="mb-1">
                SCHOOL NAME:{" "}
                <b>&nbsp; Ahmed Bin Majid Private School (ABM) - Muscat</b>
              </div>
              <div>
                GCE Exam Result: <Badge color="success">Passed</Badge>
              </div>
            </Col>
          </div>
        </Form>
      </Col>
    </Row>
  );
};

export default PersonalInfo;
