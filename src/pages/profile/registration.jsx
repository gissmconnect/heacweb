import React, { Fragment } from "react";
import { Row, Col, Form, Label, Input } from "reactstrap";
import { FirstName, LastName } from "constant";

const Registration = () => {
  return (
    <Fragment>
      <Row>
        <Col sm="12">
          <Form className="needs-validation">
            <div className="form-row">
              <Col md="12 mb-3">
                <Label>{FirstName}</Label>
                <Input
                  className="form-control"
                  name="firstName"
                  type="text"
                  placeholder="First name"
                />
                <div className="valid-feedback">{"Looks good!"}</div>
              </Col>

              <Col md="12 mb-3">
                <Label>{LastName}</Label>
                <Input
                  className="form-control"
                  name="lastName"
                  type="text"
                  placeholder="Last name"
                />
                <div className="valid-feedback">{"Looks good!"}</div>
              </Col>
            </div>
          </Form>
        </Col>
      </Row>
    </Fragment>
  );
};

export default Registration;
