import React from "react";
import { useForm } from "react-hook-form";
import { Row, Col, Form, Label, Input, Badge, FormGroup } from "reactstrap";

const StudentCategory = () => {
  const { register, handleSubmit, formState } = useForm();

  const { errors = {} } = formState;

  const onSubmit = (data) => {
    if (data !== "") {
      alert("You submitted the form and stuff!");
    } else {
      errors.showMessages();
    }
  };

  return (
    <Row className="mt-5">
      <Col>
        <Form className="needs-validation" onSubmit={handleSubmit(onSubmit)}>
          <div className="form-row mb-1">
            <Col>
              <Label for="studentCategory">* Student Category</Label>
              <FormGroup className="ml-4">
                <Label check>
                  <Input type="radio" id="studentCategory" /> Low Income Student
                  (Family income done not exceed OMR 600.00 per month).
                </Label>
                <Label check>
                  <Input type="radio" id="studentCategory" /> Social Security
                  Student (Last day to verify your social Security status is
                  before 30/7 of each year).
                </Label>
                <Label check>
                  <Input checked={true} type="radio" id="studentCategory" /> Other Type (Other
                  than Low Income & Social Security).
                </Label>
              </FormGroup>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col>
              <FormGroup className="row mt-2">
                <Label className="col-sm-3 col-form-label">
                  Social Security Number
                </Label>
                <Col sm="5">
                  <Input
                    className="form-control"
                    name="socialSecurityNumber"
                    type="text"
                    placeholder="Social Security Number"
                    {...register("socialSecurityNumber", {
                      required: true,
                    })}
                  />
                  <span className="info-text">
                    If you are from a Social Security family.
                  </span>
                </Col>
              </FormGroup>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col>
              <FormGroup className="row mt-2">
                <Label className="col-sm-3 col-form-label">
                  Do you have any disabilities?
                </Label>
                <Col sm="5">
                  <Input
                    type="select"
                    name="select"
                    className="form-control btn-pill digits"
                    id="exampleFormControlSelect7"
                  >
                    <option>{"Issue Center 1"}</option>
                    <option>{"Issue Center 2"}</option>
                    <option>{"Issue Center 3"}</option>
                    <option>{"Issue Center 4"}</option>
                    <option>{"Issue Center 5"}</option>
                    <option>{"Issue Center 6"}</option>
                  </Input>
                </Col>
              </FormGroup>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col>
              <FormGroup className="row mt-2">
                <Label className="col-sm-3 col-form-label">
                  * Social Security issue center
                </Label>
                <Col sm="5" className="ml-3">
                  <div>
                    <Label check>
                      <Input type="radio" id="studentCategory" /> Yes
                    </Label>
                  </div>
                  <div>
                    <Label check>
                      <Input checked type="radio" id="studentCategory" /> No
                    </Label>
                  </div>
                </Col>
              </FormGroup>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col>
              <FormGroup className="row mt-2">
                <Label className="col-sm-3 col-form-label">Disability</Label>
                <Col sm="5">
                  <Input
                    type="select"
                    name="select"
                    className="form-control btn-pill digits"
                    id="exampleFormControlSelect7"
                  >
                    <option>{"Disability 1"}</option>
                    <option>{"Disability 2"}</option>
                    <option>{"Disability 3"}</option>
                    <option>{"Disability 4"}</option>
                    <option>{"Disability 5"}</option>
                    <option>{"Disability 6"}</option>
                  </Input>
                </Col>
              </FormGroup>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col>
              <FormGroup className="row mt-2">
                <Label className="col-sm-3 col-form-label">
                  Other Disability
                </Label>
                <Col sm="5">
                  <Input
                    name="otherDisability"
                    placeholder="Other Disability"
                    className="form-control btn-pill digits"
                    id="exampleFormControlSelect7"
                  />
                </Col>
              </FormGroup>
            </Col>
          </div>
        </Form>
      </Col>
    </Row>
  );
};

export default StudentCategory;
