import React, { Fragment } from "react";
import { Breadcrumb } from "components";
import StepZilla from "react-stepzilla";
import Birthdate from "./birthdate";
import { Container, Row, Col, Card, CardBody } from "reactstrap";
import PersonalInfo from "./personalInfo";
import Address from "./address";
import StudentCategory from "./studentCategory";
import ContactInfo from "./contactInfo";
import OtherInfo from "./otherInfo";
import EditProfileTips from "./tips";

const FormWizard = () => {
  const steps = [
    { name: "Personal Info", component: <PersonalInfo /> },
    { name: "Address", component: <Address /> },
    { name: "Category Info", component: <StudentCategory /> },
    { name: "Contact Info", component: <ContactInfo /> },
    { name: "Other Info", component: <OtherInfo /> },
  ];
  return (
    <Fragment>
      <Breadcrumb parent="" title="Edit Profile" />
      <Col>
        <EditProfileTips />
      </Col>
      <Container fluid={true}>
        <Row>
          <Col sm="12">
            <Card>
              <CardBody className="pt-4">
                <StepZilla
                  steps={steps}
                  showSteps={true}
                  showNavigation={true}
                  stepsNavigation={true}
                  prevBtnOnLastStep={true}
                  dontValidate={true}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
};

export default FormWizard;
