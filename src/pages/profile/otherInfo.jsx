import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { Row, Col, Form, Label, Input, FormGroup } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const OtherInfo = () => {
  const [startDate, setStartDate] = useState(new Date());

  const { register, handleSubmit, formState } = useForm();

  const { errors = {} } = formState;

  const handleChange = (date) => {
    setStartDate(date);
  };

  const onSubmit = (data) => {
    if (data !== "") {
      alert("You submitted the form and stuff!");
    } else {
      errors.showMessages();
    }
  };

  return (
    <Row className="mt-5">
      <Col>
        <Form className="needs-validation" onSubmit={handleSubmit(onSubmit)}>
          <div className="form-row mb-1">
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  ID Card Expiry Date
                </Label>
                <Col sm="7">
                  <DatePicker
                    className="form-control digits"
                    selected={startDate}
                    onChange={handleChange}
                  />
                </Col>
              </FormGroup>
            </Col>
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Marital Status
                </Label>
                <Col sm="7">
                  <Input
                    type="select"
                    name="maritalStatus"
                    className="form-control btn-pill digits"
                  >
                    <option>{"Married"}</option>
                    <option>{"Single"}</option>
                  </Input>
                </Col>
              </FormGroup>
            </Col>
          </div>
          <div className="form-row mb-1">
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Place of Birth
                </Label>
                <Col sm="7">
                  <Input
                    type="select"
                    name="birthPlace"
                    className="form-control btn-pill digits"
                  >
                    <option>{"Oman"}</option>
                    <option>{"USA"}</option>
                    <option>{"Mexico"}</option>
                  </Input>
                </Col>
              </FormGroup>
            </Col>
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Birth Wilayat Name
                </Label>
                <Col sm="7">
                  <Input
                    type="select"
                    name="birthWilayatName"
                    className="form-control btn-pill digits"
                  >
                    <option>{"Sohar"}</option>
                    <option>{"Oman"}</option>
                  </Input>
                </Col>
              </FormGroup>
            </Col>
          </div>

          <div className="form-row mb-1">
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Medical Issue
                </Label>
                <Col sm="5" className="ml-3">
                  <div>
                    <Label check>
                      <Input type="radio" id="studentCategory" /> Yes
                    </Label>
                  </div>
                  <div>
                    <Label check>
                      <Input checked type="radio" id="studentCategory" /> No
                    </Label>
                  </div>
                </Col>
              </FormGroup>
            </Col>
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">* Religion</Label>
                <Col sm="7">
                  <Input
                    type="select"
                    name="birthWilayatName"
                    className="form-control btn-pill digits"
                  >
                    <option>{""}</option>
                    <option>{""}</option>
                  </Input>
                </Col>
              </FormGroup>
            </Col>
          </div>

          <div className="form-row mb-1">
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Guardian Name
                </Label>
                <Col sm="7">
                  <Input
                    className="form-control"
                    name="guardianName"
                    type="text"
                    placeholder="Guardian Name"
                    {...register("guardianName", { required: true })}
                  />
                  <span>{errors.guardianName && "Email is required"}</span>
                </Col>
              </FormGroup>
            </Col>
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Guardian Relationship
                </Label>
                <Col sm="7">
                  <Input
                    type="select"
                    name="guardianRelationship"
                    className="form-control btn-pill digits"
                  >
                    <option>{"Father"}</option>
                    <option>{"Mother"}</option>
                    <option>{"Brother"}</option>
                    <option>{"Sister"}</option>
                  </Input>
                </Col>
              </FormGroup>
            </Col>
          </div>

          <div className="form-row mb-1">
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Guardian Address
                </Label>
                <Col sm="7">
                  <Input
                    className="form-control"
                    name="guardianAddress"
                    type="text"
                    placeholder="Guardian Address"
                    {...register("guardianAddress", { required: true })}
                  />
                  <span>
                    {errors.guardianAddress && "Guardian Address is required"}
                  </span>
                </Col>
              </FormGroup>
            </Col>
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  * Guardian Work Place
                </Label>
                <Col sm="7">
                  <Input
                    className="form-control"
                    name="guardianWorkPlace"
                    type="text"
                    placeholder="Guardian Work Place"
                    {...register("guardianWorkPlace", { required: true })}
                  />
                  <span>
                    {errors.guardianAddress &&
                      "Guardian Work Place is required"}
                  </span>
                </Col>
              </FormGroup>
            </Col>
          </div>

          <div className="form-row mb-1">
            <Col sm="6">
              <FormGroup className="row mt-2">
                <Label className="col-sm-4 col-form-label">
                  Guardian Work Phone
                </Label>
                <Col sm="7">
                  <Input
                    className="form-control"
                    name="guardianWorkPhone"
                    type="text"
                    placeholder="Guardian Work Phone"
                    {...register("guardianWorkPhone", { required: true })}
                  />
                </Col>
              </FormGroup>
            </Col>
          </div>
        </Form>
      </Col>
    </Row>
  );
};

export default OtherInfo;
