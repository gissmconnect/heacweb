import { Tips } from "components";
import React from "react";
import { translate } from "react-switch-lang";

const EditProfileTips = () => {
  return (
    <Tips>
      <p>You can alter your contact information any time.</p>
      <p>
        Make sure that your willayat name is correct as some programmes are allocated according to student willayat.
      </p>
      <br />
      <p>
        The total income of (Low income) students should not exceed 600 OR, and higher than this amount cannot be accepted permanently.
      </p>
    </Tips>
  );
};

export default translate(EditProfileTips);
