import React, { Fragment } from "react";
import { Breadcrumb } from "components";
import { translate } from "react-switch-lang";
import Programs from "./programs";
import { Col } from "reactstrap";
import ChooseProgramsTips from "./tips";

const ChoosePrograms = (props) => {
  return (
    <Fragment>
      <Breadcrumb
        parent=""
        title="Application for Public Programs, Scholarship and Grants"
      />
      <Col>
        <ChooseProgramsTips />
      </Col>
      <Programs />
    </Fragment>
  );
};

export default translate(ChoosePrograms);
