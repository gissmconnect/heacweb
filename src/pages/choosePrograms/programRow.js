// @ts-nocheck
import React from "react";
import { Draggable } from "react-beautiful-dnd";
import { X } from "react-feather";
import ProgramDetailPopup from "./programDetailPopup";

const renderTemporyRow = (program, index) => (
  <tr>
    <th scope="row">{index + 1}</th>
    <td>{program.code}</td>
    <td>{program.name}</td>
    <td>
      <ProgramDetailPopup program={program} />
    </td>
  </tr>
);

const ProgramRow = (props) => {
  const { program = {}, columnIndex, index } = props;

  const isWishlist = !!columnIndex;
  const sn = index + 1; // Serial Number

  if (props.isEmpty) {
    return <div className="empty-list">You haven't any item in wishlist</div>;
  }

  return (
    <>
      <Draggable
        key={program.id}
        draggableId={program.id}
        index={index}
      >
        {(provided, snapshot) => (
          <>
            {snapshot.isDragging && !isWishlist
              ? renderTemporyRow(program, index)
              : null}
            <tr
              className={`${snapshot.isDragging ? " activeRow" : ""} ${
                props.className
              }`}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              <th scope="row">{sn ? sn : ""}</th>
              <td>{program.code}</td>
              <td className="program-name-td">{program?.name}</td>
              <td>{sn ? <ProgramDetailPopup program={program} /> : ""}</td>
              {isWishlist ? (
                <td className="delete-icon-td">
                  <X
                    onClick={() =>
                      props.removeProgram(props.columnId, program.id)
                    }
                  />
                </td>
              ) : null}
            </tr>
          </>
        )}
      </Draggable>
    </>
  );
};

export default ProgramRow;
