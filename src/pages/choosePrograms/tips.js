import React from "react";
import { translate } from "react-switch-lang";
import { Tips } from "components";

const ChooseProgramsTips = () => {
  return (
    <Tips>
      <p>Your Programs Choices as recorded in HEAC are shown below.</p>
      <br />
      <p>
        If you wish, you may change your choices any time before the last date.
      </p>
      <br />
      <p>For reference, click the 'Print' button to print this page.</p>
      <br />
      <p>This record does not confirm that you are offered any Program.</p>
      <br />
      <p>
        When available, you will be offered your highest possible preference of
        program.
      </p>
    </Tips>
  );
};

export default translate(ChooseProgramsTips);
