import { Edit, Home, FileText, MessageSquare, LogIn, AlignLeft, Award, FilePlus, Upload, Bookmark } from "react-feather";
export const MENUITEMS = [
  {
    Items: [
      {
        title: "Home",
        icon: Home,
        type: "link",
        active: false,
        path: `${process.env.PUBLIC_URL}`,
      },
    ],
  },
  {
    Items: [
      {
        title: "Change_Personal_Details",
        icon: Edit,
        type: "link",
        active: false,
        path: `${process.env.PUBLIC_URL}/profile`,
      },
    ],
  },
  {
    Items: [
      {
        title: "View_Marks_Grades_List",
        icon: FileText,
        type: "link",
        active: false,
        path: `${process.env.PUBLIC_URL}/marks-grades`,
      },
    ],
  },
  {
    Items: [
      {
        title: "SMS_Inbox",
        icon: MessageSquare,
        type: "link",
        active: false,
        path: `${process.env.PUBLIC_URL}/inbox`,
      },
    ],
  },
  {
    Items: [
      {
        title: "Choose Programs",
        icon: Bookmark,
        type: "link",
        active: false,
        path: `${process.env.PUBLIC_URL}/choose-programs`,
      },
    ],
  },
  {
    Items: [
      {
        title: "Eligible Programs",
        icon: AlignLeft,
        type: "link",
        active: false,
        path: `${process.env.PUBLIC_URL}/eligible-programs`,
      },
    ],
  },
  {
    Items: [
      {
        title: "View and Accept Offers",
        icon: Award,
        type: "link",
        active: false,
        path: `${process.env.PUBLIC_URL}/view-accept-offers`,
      },
    ],
  },
  {
    Items: [
      {
        title: "File Upload",
        icon: Upload,
        type: "link",
        active: false,
        path: `${process.env.PUBLIC_URL}/file-upload`,
      },
    ],
  },
  {
    Items: [
      {
        title: "Sign_Out",
        icon: LogIn,
        type: "link",
        active: false,
        path: `${process.env.PUBLIC_URL}`,
      },
    ],
  },
];
