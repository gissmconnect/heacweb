import React, { Fragment, useState } from "react";
import man from "assets/images/dashboard/profile.jpg";
import { LogIn, User, Minimize, MessageSquare } from "react-feather";
import {
  setTranslations,
  setDefaultLanguage,
  setLanguageCookie,
  setLanguage,
  translate,
} from "react-switch-lang";
import { English, EricaHughes, KoriThomas, AinChavez } from "constant";
import en from "assets/i18n/en.json";
import ar from "assets/i18n/ar.json";
import { setLayoutLtr, setLayoutRtl } from "layout/theme-customizer";

setTranslations({ en, ar });
setDefaultLanguage("en");
setLanguageCookie();

const Rightbar = (props) => {
  const [langdropdown, setLangdropdown] = useState(false);
  const [selected, setSelected] = useState("en");
  const [chatDropDown, setChatDropDown] = useState(false);

  const handleSetLanguage = (key) => {
    setLanguage(key);
    setSelected(key);
    if (key === "ar") {
      setLayoutRtl();
    } else {
      setLayoutLtr();
    }
  };

  //full screen function
  function goFull() {
    if (
      (document.fullScreenElement && document.fullScreenElement !== null) ||
      (!document.mozFullScreen && !document.webkitIsFullScreen)
    ) {
      if (document.documentElement.requestFullScreen) {
        document.documentElement.requestFullScreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullScreen) {
        document.documentElement.webkitRequestFullScreen(
          Element.ALLOW_KEYBOARD_INPUT
        );
      }
    } else {
      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
    }
  }

  const LanguageSelection = (language) => {
    if (language) {
      setLangdropdown(!language);
    } else {
      setLangdropdown(!language);
    }
  };

  return (
    <Fragment>
      <div className="nav-right col-8 pull-right right-header p-0">
        <ul className="nav-menus">
          <li className="language-nav">
            <div
              className={`translate_wrapper ${langdropdown ? "active" : ""}`}
            >
              <div className="current_lang">
                <div
                  className="lang"
                  onClick={() => LanguageSelection(langdropdown)}
                >
                  <i
                    className={`flag-icon flag-icon-${
                      selected === "en"
                        ? "us"
                        : selected === "ar"
                        ? "eg"
                        : selected
                    }`}
                  ></i>
                  <span className="lang-txt">{selected}</span>
                </div>
              </div>
              <div className={`more_lang ${langdropdown ? "active" : ""}`}>
                <div className="lang" onClick={() => handleSetLanguage("en")}>
                  <i className="flag-icon flag-icon-us"></i>
                  <span className="lang-txt">
                    {English}
                    <span> {"(US)"}</span>
                  </span>
                </div>
                <div className="lang" onClick={() => handleSetLanguage("ar")}>
                  <i className="flag-icon flag-icon-eg"></i>
                  <span className="lang-txt">
                    {"عربي"}
                    <span> {"(ar)"}</span>
                  </span>
                </div>
              </div>
            </div>
          </li>
          <li
            className="onhover-dropdown"
            onClick={() => setChatDropDown(!chatDropDown)}
          >
            <MessageSquare />
            <ul
              className={`chat-dropdown onhover-show-div ${
                chatDropDown ? "active" : ""
              }`}
            >
              <li>
                <MessageSquare />
                <h6 className="f-18 mb-0">{props.t("SMS_Inbox")}</h6>
              </li>
              <li>
                <div className="media">
                  <div className="media-body">
                    <span>{EricaHughes}</span>
                    <p>{"Lorem Ipsum is simply dummy..."}</p>
                  </div>
                  <p className="f-12 font-success">{"58 mins ago"}</p>
                </div>
              </li>
              <li>
                <div className="media">
                  <div className="media-body">
                    <span>{KoriThomas}</span>
                    <p>{"Lorem Ipsum is simply dummy..."}</p>
                  </div>
                  <p className="f-12 font-success">{"1 hr ago"}</p>
                </div>
              </li>
              <li>
                <div className="media">
                  <div className="media-body">
                    <span>{AinChavez}</span>
                    <p>{"Lorem Ipsum is simply dummy..."}</p>
                  </div>
                  <p className="f-12 font-danger">{"32 mins ago"}</p>
                </div>
              </li>
              <li className="text-center">
                {" "}
                <a href={`${process.env.PUBLIC_URL}`}>
                  <button className="btn btn-primary">
                    {props.t("View_All")}{" "}
                  </button>
                </a>
              </li>
            </ul>
          </li>
          <li className="maximize">
            <a className="text-dark" href="#javascript" onClick={goFull}>
              <Minimize />
            </a>
          </li>
          <li className="profile-nav onhover-dropdown p-0">
            <div className="media profile-media">
              <img className="b-r-10" src={man} alt="" />
              <div className="media-body">
                <span>{"Tamadher - Ghafri"}</span>
                <p className="mb-0 font-roboto">
                  {props.t("Student")}{" "}
                  <i className="middle fa fa-angle-down"></i>
                </p>
              </div>
            </div>
            <ul className="profile-dropdown onhover-show-div">
              <li>
                <User />
                <span>{props.t("Profile")} </span>
              </li>
              <li>
                <LogIn />
                <span>{props.t("Logout")}</span>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </Fragment>
  );
};
export default translate(Rightbar);
