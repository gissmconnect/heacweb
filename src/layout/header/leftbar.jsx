import React, { Fragment } from "react";
import { Col } from "reactstrap";
import { translate } from 'react-switch-lang';

const Leftbar = (props) => {
  return (
    <Fragment>
      <Col className="left-header horizontal-wrapper pl-0">
        <h6>{props.t("Title")}</h6>
      </Col>
    </Fragment>
  );
};

export default translate(Leftbar);
