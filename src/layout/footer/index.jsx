import React, { Fragment } from "react";
import { translate } from "react-switch-lang";
import { Container, Row, Col } from "reactstrap";

const Footer = (props) => {
  return (
    <Fragment>
      <footer className="footer">
        <Container fluid={true}>
          <Row>
            <Col md="12" className="footer-copyright text-center">
              <p className="mb-0">{`${props.t(
                "All_Rights_Reserved"
              )} ${new Date().getFullYear()}. ${props.t(
                "Powered_By"
              )} ${props.t("Title_Short")}`}</p>
            </Col>
          </Row>
        </Container>
      </footer>
    </Fragment>
  );
};

export default translate(Footer);
