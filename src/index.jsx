import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "components/app";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import store from "./store";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { routes } from "route";
import ConfigDB from "data/customizer/config";

// @ts-ignore
const Root = (props) => {
  const abortController = new AbortController();

  useEffect(() => {
    // @ts-ignore
    console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"];
    // @ts-ignore
    console.disableYellowBox = true;
    return function cleanup() {
      abortController.abort();
    };
  }, [abortController]);

  return (
    <Provider store={store}>
      <BrowserRouter basename={`/`}>
        <Switch>
          <App>
            <TransitionGroup>
              {routes.map(({ path, Component }) => (
                <Route
                  key={path}
                  exact
                  path={`${process.env.PUBLIC_URL}${path}`}
                >
                  {/* {({ match }) => (
                    <CSSTransition
                      in={match != null}
                      timeout={100}
                      classNames={ConfigDB.data.router_animation}
                      unmountOnExit
                    > */}
                      <div>
                        <Component />
                      </div>
                    {/* </CSSTransition>
                  )} */}
                </Route>
              ))}
            </TransitionGroup>
          </App>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
};
ReactDOM.render(<Root />, document.getElementById("root"));

serviceWorker.unregister();
